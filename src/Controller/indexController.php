<?php

namespace Controller;

class indexController extends baseController {

    public function landingpage($params) {
        
    }

    /**
     * 
     * @param type $params
     * @return type
     */
    public function contact($params) {

        $form = $this->app['form.factory']
                ->createBuilder('form')
                ->add('name', 'text', array('label' => 'Name:'))
                ->add('email', 'email', array('label' => 'Email:'))
                ->add('message', 'textarea', array('label' => 'Message:'))
                ->getForm();

        if ('POST' == $this->app['request']->getMethod()) {
            $form->bindRequest($this->app['request']);
            if ($form->isValid()) {
                $data = $form->getData();

                \Swift_Mailer::newInstance(\Swift_MailTransport::newInstance())
                        ->send(\Swift_Message::newInstance()
                                ->setSubject(sprintf('Contact from %s', $_SERVER['SERVER_NAME']))
                                ->setFrom(array($data['email']))
                                ->setTo(array('umpirsky@gmail.com'))
                                ->setBody($data['message'])
                );

                return $this->app->redirect($this->app['url_generator']->generate(
                                        'contact', array('sent' => true)
                ));
            }
        }

        return array(
            'form' => $form->createView()
        );
    }

}