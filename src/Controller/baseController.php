<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;

class baseController {

    public $isAjax = false;
    public $app = null;

    /**
     * init app & deps
     * @param type $app
     * @return type
     */
    public function __construct($app) {
        $this->app = $app;

        $app->before(function () use ($app) {
                    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('/layout/page.html.twig'));
                });
        // Register error handlers
        $app->error(
                function (\Exception $e) use ($app) {
                    var_dump($e->getMessage());
                    if ($e instanceof NotFoundHttpException) {
                        return $app['twig']->render('pages/error.twig', array('code' => 404));
                    }

                    $code = ($e instanceof HttpException) ? $e->getStatusCode() : 500;
                    return $app['twig']->render('pages/error.twig', array('code' => $code));
                }
        );

        $app->register(new ValidatorServiceProvider());
        $app->register(new FormServiceProvider());
        $app->register(new TranslationServiceProvider());

        $app->register(new TwigServiceProvider(), array(
            'twig.path' => array(__DIR__ . '/../../views'),
                //'twig.options' => array('cache' => __DIR__.'/../cache/twig'),
        ));

        $app->register(new SwiftmailerServiceProvider());

        $app['swiftmailer.options'] = array(
            'host' => 'smtp.gmail.com',
            'port' => 465,
            'username' => 'silex.swiftmailer@gmail.com',
            'password' => 'simplepassword',
            'encryption' => 'ssl',
            'auth_mode' => 'login'
        );
    }

    /**
     *
     * @param <type> $dirty
     * @return <type>
     */
    public function _cleanRequest($dirty = array()) {
        $clean_array = array();
        foreach ($dirty as $key => $val) {
            if (!is_array($val)) {
                $clean_array[$key] = mysql_escape_string($val);
            } else {
                $clean_array[$key] = $this->_cleanRequest($val);
            }
        }
        unset($clean_array['ZDEDebuggerPresent']);
        unset($clean_array['PHPSESSID']);

        return $clean_array;
    }

    /**
     * 
     * @return type
     */
    public function _getParams() {
        $this->isAjax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

        return $this->_cleanRequest($_REQUEST + $_GET + $_POST);
    }

    /**
     * 
     * @param type $tplParams
     * @return type
     */
    public function _render($tplParams) {
        if (!$this->isAjax) {
            $app = $this->app;
            // Add layout

            $tplParams['view_base'] = (isset($tplParams['view_base'])) ? $tplParams['view_base'] : 'page';

            $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('/layout/' . $tplParams['view_base'] . '.html.twig'));

            return $this->app['twig']->render('pages/' . $tplParams['view_template'] . '.html.twig', $tplParams);
        } else {
            unset($tplParams['view_template']);
            header('Content-Type: application/json');
            echo json_encode($tplParams);
            exit();
        }
    }

}