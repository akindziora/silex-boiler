<?php

use Silex\Application;

$app = new Silex\Application();

// Add pages
$routes = require_once "config/routes.php";

foreach ($routes as $route => $view) {
    $app->get($route, function () use ($app, $view) {
                $contrAction = explode(':', $view);

                //include_once('controller/' . $contrAction[0] . '.php');

                $cntrlName = "\Controller\\{$contrAction[0]}Controller";

                $contrlr = new $cntrlName($app);

                $tplParams = (array) $contrlr->{$contrAction[1]}($contrlr->_getParams());

                if (!isset($tplParams['view_template'])) {
                    $tplParams['view_template'] = $contrAction[1];
                }
                return $contrlr->_render($tplParams);
            })->bind($view);
}

// Run
$app->run();